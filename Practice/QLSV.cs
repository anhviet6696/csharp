﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class QLSV
    {
        public SV[] listSV { get; set; }
        public int Count { get; set; }
        public QLSV()
        {
            this.listSV = null;
            this.Count = 0;
        }


        public void Add(SV s)
        {
            if (this.Count == 0)
            {
                this.listSV = new SV[this.Count + 1];
                this.listSV[0] = s;
            }
            else
            {
                SV[] temp = new SV[this.Count];
                for (int i = 0; i < this.Count; i++)
                {
                    temp[i] = this.listSV[i];
                }
                this.listSV = new SV[this.Count + 1];
                for (int i = 0; i < this.Count; ++i)
                {
                    this.listSV[i] = temp[i];
                }
                this.listSV[this.Count] = s;// add
            }

            this.Count++;

        }
        public void Insert(int index, SV s)
        {
            if (this.Count == 0) // mang trong thi add vao luon
            {
                Add(s);
            }
            else
            {
                if (index == this.Count)
                {
                    Add(s);
                }
                else if (index==0 || 0 <index && index < this.Count) //k nam trong khoang tu 0 toi count nhung ko=0 va count
                {
                    // ve nha code tiep

                }
            }

        }
        public void Remove(SV s)
        {
            int index = IndexOf(s);
            if (index != -1)
            {
                RemoveAt(index);
            }
        }
        public void RemoveAt(int index)
        {
            SV[] temp = new SV[this.Count];
            for (int i = 0; i < this.Count; i++) // sao chep lai toan bo mang
            {
                temp[i] = this.listSV[i];
            }
            this.listSV = new SV[this.Count - 1];// khoi tao mang moi nho hon mang ban dau 1 phan tu
            for (int i = 0; i < this.Count; i++)
            {
                if (i < index)
                {
                    this.listSV[i] = temp[i];
                }
                else if (i >= index)
                {
                    for (int j = i; j < this.Count - 1; j++)
                    {
                        this.listSV[j] = temp[j + 1];
                    }
                    break;
                }
            }
            this.Count--;
        }

        public static bool IsEquals(SV s1, SV s2)
        {
            return (s1.MSSV == s2.MSSV && s1.Name == s2.Name && s1.Age == s2.Age);
        }

        public int IndexOf(SV s)
        {
            for (int i = 0; i <= Count - 1; i++) // hoac i< count
            {
                if (IsEquals(this.listSV[i], s))
                {
                    return i;
                }
            }
            return -1;
        }
        public void Sort()
        {
            SV[] temp = new SV[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                for (int j = i + 1; j < this.Count; j++)
                {
                    if (this.listSV[i].Name.CompareTo(this.listSV[j].Name) > 0)
                    {
                        temp[j] = this.listSV[i];
                        this.listSV[i] = this.listSV[j];
                        this.listSV[j] = temp[j];
                    }
                }
            }
            foreach (SV i in this.listSV)
            {
                Console.WriteLine(i);
            }
        }
        public void Reverse()
        {
            SV[] temp = new SV[this.Count];
            int j = this.Count;
            for (int i = 0; i < this.Count; i++)
            {
                temp[j - 1] = this.listSV[i];
                j = j - 1;
            }
            foreach (SV i in temp)
            {
                Console.WriteLine(i);
            }
        }
    }
}