﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    public class SV
    {

        public string MSSV { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }



        public override string ToString()
        {
            return (this.MSSV + this.Name + this.Age);           
        }
        public override bool Equals(object obj) // so sanh doi tuong truyen vao voi this hien tai
        {
            if (this.MSSV == ((SV)obj).MSSV &&
                this.Name == ((SV)obj).Name &&
                this.Age == ((SV)obj).Age)
            {
                return true;
            }
            else return false;
        }
        public void Show()
        {
            Console.WriteLine("SV");
        }
        // dùng cho vấn đề 2
        //public static bool CompareAge(SV o1,SV o2)
        //{
        //    if(o1.Age > o2.Age)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        //public static bool CompareMSSV(SV o1, SV o2)
        //{
        //    if (String.Compare(o1.Name,o2.Name)>0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        //public static bool CompareName(SV o1, SV o2)
        //{
        //    if (String.Compare(o1.Name, o2.Name) > 0)// trả về  kiểu int nếu > 0 thì chuỗi trước > sau, ngược lại và =0 thì 2 chuỗi giống nhau

        //    {
        //        return true;
        //    }
        //    return false;
        //}

            // dùng cho vấn đề 1
        public static bool CompareAge(object o1, object o2)
        {
            if (((SV)o1).Age > ((SV)o2).Age)
            {
                return true;
            }
            return false;
        }
        public static bool CompareMSSV(object o1, object o2)
        {
            if (String.Compare(((SV)o1).Name, ((SV)o2).Name) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareName(object o1, object o2)
        {
            if (String.Compare(((SV)o1).Name, ((SV)o2).Name) > 0)// trả về  kiểu int nếu > 0 thì chuỗi trước > sau, ngược lại và =0 thì 2 chuỗi giống nhau

            {
                return true;
            }
            return false;
        }
    }
}
