﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Program
    {
        // hàm sort sắp xếp thứ tự các sinh viên theo thứ tự  age
        // 1) Sắp xếp mảng các đối tượng có kiểu dữ liệu bất kì
        // 2) Sắp xếp mảng theo 1 thuộc tính bất kì

        //public delegate bool CompareObj(SV o1, SV o2); // giải quyết vấn đề 2
        //static void Sort(SV[] arrs, CompareObj cmp)
        //{

        //    for (int i = 0; i < arrs.Length; i++)
        //    {
        //        for (int j = i + 1; j < arrs.Length; j++)
        //        {
        //            //if (arrs[i].Age > (arrs[j].Age))
        //            if (cmp(arrs[i], arrs[j]))
        //            {
        //                SV temp = arrs[i];
        //                arrs[i] = arrs[j];
        //                arrs[j] = temp;
        //            }
        //        }
        //    }
        //    foreach (SV i in arrs)
        //    {
        //        Console.WriteLine(i);
        //    }
        //}
        public delegate bool CompareObj(object o1, object o2); // giải quyết vấn đề 1
        static void Sort(object[] arrss, CompareObj cmp)
        {

            for (int i = 0; i < arrss.Length; i++)
            {
                for (int j = i + 1; j < arrss.Length; j++)
                {
                    //if (arrss[i].Age > (arrss[j].Age))
                    if (cmp(arrss[i], arrss[j]))
                    {
                        object temp = arrss[i];
                        arrss[i] = arrss[j];
                        arrss[j] = temp;
                    }
                }
            }
            
        }
        static void Main(string[] args)
        {
            //SV s1 = new SV { MSSV = "1", Name = "Quoc Viet", Age = 18 };
            //SV s2 = new SV { MSSV = "1", Name = "Quoc Viet", Age = 18 };
            //Console.WriteLine(s1.ToString());
            //Console.WriteLine(s1.Equals(s2));i
            //Console.ReadKey();


            //List<SV> listSV = new List<SV>();

            QLSV s1 = new QLSV();

            SV a1 = new SV { MSSV = "1", Name = "Nguyen Van Binh", Age = 21 };
            SV a2 = new SV { MSSV = "2", Name = "Nguyen Van Anh", Age = 18 };
            SV a3 = new SV { MSSV = "3", Name = "Nguyen Van Tinh", Age = 16 };
            SV a4 = new SV { MSSV = "4", Name = "Nguyen Van Khanh", Age = 51 };
            s1.Add(a1);
            s1.Add(a2);
            s1.Add(a3);

            //foreach (SV i in s1.listSV)
            //{
            //    Console.WriteLine(i);
            //}

            //Console.WriteLine( "Chi so cua phan tu : ={0}",s1.IndexOf(a2));
            //s1.RemoveAt(a1);
            //s1.Remove(a2);
            //s1.Reverse();
            //s1.Sort();
            s1.Insert(3, a4);

            //li do tai sao bien i trong foreach (cach dung bien i trong foreach co ghi cach dung trong vo)
            SV[] arr = { a1, a2 };
            foreach (SV i in arr)
            {
                if (i.MSSV == "2")
                {
                    //i=s3;
                    i.Name = "DHP";
                }
            }
            foreach (SV i in arr)
            {
                Console.WriteLine(i.ToString());
            }
            //doan code nay minh hoa cho viec dung New va Override trong da hinh
            SVFU ss = new SVFU { MSSV = "1", Name = "Viet", Age = 14, HOL = true };
            a1.Show();
            ss.Show();


            // doan code nay da hinh IFile vs IFileTwo va da hinh ca 4 method
            IFileTwo isf = (IFileTwo)new MyFile(); // ko dc khoi tao doi tuong cu the thuoc ve interface IFileTwo
            isf.disFile();

            // doan code nay da hinh chi 3 method
            MyFile1 mf = new MyFile1();
            mf.disFile();



            // đoạn code này dùng cho hàm Sort ở trên
            SV[] arrs = { a1, a2, a3, a4 };
            //Sort(arrs, SV.CompareAge);

            Person p1=new Person { CMND="201813368" };
            Person p2 = new Person { CMND = "201813369" };
            Person[] arrss={p1,p2};
            // khai bao mang Person
            Sort(arrss, Person.CompareCMND);
            foreach (Person i in arrss)
            {
                Console.WriteLine(i.CMND);
            }
            Console.ReadKey();
        }
    }
}
