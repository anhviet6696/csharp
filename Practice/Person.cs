﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Person
    {
        public string CMND { get; set; }
        
        public static bool CompareCMND(object o1, object o2)
        {
            if (string.Compare(((Person)o1).CMND , ((Person)o2).CMND) >0)
            {
                return true;
            }
            return false;
        }
    }
}
