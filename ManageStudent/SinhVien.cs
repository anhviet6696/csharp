﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudent
{
    class SinhVien
    {
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string HoTen { get; set; }
        public string MaSo { get; set; }
        public string NgaySinh { get; set; }
        public string NienKhoa { get; set; }

        public SinhVien() { }

        public SinhVien(string DiaChi, string DienThoai, string HoTen, string MaSo,
            DateTime NgaySinh, string NienKhoa)
        {
            this.DiaChi = DiaChi;
            this.DienThoai = DienThoai;
            this.HoTen = HoTen;
            this.MaSo = MaSo;
            this.NgaySinh = NgaySinh.ToString();
            this.NienKhoa = NienKhoa;

        }
        public virtual string LoaiHinh()
        {
            return null;
        }
    }
}
