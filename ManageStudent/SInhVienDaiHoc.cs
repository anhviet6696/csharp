﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudent
{
    class SinhVienDaiHoc : SinhVien
    {

        public string ChuyenNganh { get; set; }

        public SinhVienDaiHoc() { }

        public SinhVienDaiHoc(string DiaChi, string DienThoai, string HoTen, string MaSo,
            DateTime NgaySinh, string NienKhoa,string ChuyenNganh) :
            base(DiaChi, DienThoai, HoTen, MaSo,NgaySinh,  NienKhoa)
        {
            this.ChuyenNganh = ChuyenNganh;
        }

        public override string LoaiHinh()
        {
            return "đại học";
        }

    }
}
