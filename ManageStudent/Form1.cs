﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManageStudent
{
    public partial class Form1 : Form
    {
        ArrayList StudentList = new ArrayList();
        DataTable dt = new DataTable();
        SinhVien sv = new SinhVien();
        public Form1()
        {
            InitializeComponent();
            dt.Columns.Add("MSSV", typeof(string));
            dt.Columns.Add("Họ tên", typeof(string));
            dt.Columns.Add("Ngày sinh", typeof(string));
            dt.Columns.Add("Địa chỉ", typeof(string));
            dt.Columns.Add("Điện thoại", typeof(string));
            dt.Columns.Add("Niên khóa", typeof(string));
            dt.Columns.Add("Loại hình", typeof(string));
            dt.Columns.Add("Khác", typeof(string));
            dataGridView1.DataSource = dt;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }
        public void Reset()
        {
            txtHoTen.Text = "";
            txtMSSV.Text = "";
            txtDiaChi.Text = "";
            txtDienThoai.Text = "";
            txtNienKhoa.Text = "";
            txtBang1.Text = "";
            txtCty.Text = "";
            rDaiHoc.Checked = false;
            rBang2.Checked = false;
            rCaoDang.Checked = false;
            cbCN.Text = "";
        }
        private void btThem_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            r["Họ tên"] = txtHoTen.Text;
            r["MSSV"] = txtMSSV.Text;
            r["Ngày sinh"] = dtpNS.Value;
            r["Địa chỉ"] = txtDiaChi.Text;
            r["Điện thoại"] = txtDienThoai.Text;
            r["Niên khóa"] = txtNienKhoa.Text;
            r["Khác"] = txtNienKhoa.Text;
            if (rDaiHoc.Checked == false && rBang2.Checked == false &&
                rCaoDang.Checked == false)
            {
                MessageBox.Show("Vui long chọn hệ học!");
            }
            else
            {
                if (rDaiHoc.Checked == true)
                {
                    sv = new SinhVienDaiHoc();
                    r["Loại hình"] = sv.LoaiHinh().Trim();
                    if (cbCN.Text == "")
                    {
                        MessageBox.Show("Mời nhập chuyên ngành!");
                        return;
                    }
                    else
                    {
                        //r["Khác"] = "CNgành: " + cbCN.SelectedItem.ToString();
                        sv = new SinhVienDaiHoc(txtDiaChi.Text, txtDienThoai.Text, txtHoTen.Text,
                            txtMSSV.Text, dtpNS.Value, txtNienKhoa.Text, cbCN.Text);
                        StudentList.Add(sv);
                    }
                }
                else if (rBang2.Checked == true)
                {
                    sv = new SinhVienBangHai();
                    r["Loại hình"] = sv.LoaiHinh().Trim();
                    if (txtBang1.Text == "" || txtCty.Text == "")
                    {
                        MessageBox.Show("Mời nhập đủ thông tin!");
                        return;
                    }
                    else
                    {
                        //r["Khác"] = "B1:" + txtBang1.Text + "- Cty: " + txtCty.Text;
                        sv = new SinhVienBangHai(txtDiaChi.Text, txtDienThoai.Text, txtHoTen.Text,
                            txtMSSV.Text, dtpNS.Value, txtNienKhoa.Text, txtBang1.Text, txtCty.Text);
                        StudentList.Add(sv);
                    }
                }
                else
                {
                    sv = new SinhVienCaoDang(txtDiaChi.Text, txtDienThoai.Text, txtHoTen.Text,
                            txtMSSV.Text, dtpNS.Value, txtNienKhoa.Text);
                    StudentList.Add(sv);

                }
                dataGridView1.DataSource = StudentList;
                UpdateList();
                dt.Rows.Add(r);
                dataGridView1.DataSource = dt;
                Reset();
            }
        }
        public void UpdateList()
        {
            DataRow r = dt.NewRow();

            //dataGridView1.Rows.Clear();
            //int i = 0;
            foreach (SinhVien k in StudentList)
            {
                r["Họ tên"] = k.HoTen.Trim();
                r["MSSV"] = k.MaSo.Trim();
                r["Ngày sinh"] = k.NgaySinh.Trim();
                r["Địa chỉ"] = k.DiaChi.Trim();
                r["Điện thoại"] = k.DienThoai.Trim();
                r["Niên khóa"] = k.NienKhoa.Trim();
                r["Loại hình"] = k.LoaiHinh().Trim();
                
            }
            
            
        }
        private void btReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void btXoa_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dr = dataGridView1.SelectedRows;
            foreach (DataGridViewRow i in dr)
            {
                String ms = dataGridView1.SelectedCells[0].Value.ToString();
                foreach (SinhVien sv in StudentList)
                {
                    //int selectedIndex = dataGridView1.CurrentCell.RowIndex;
                    //if (selectedIndex > -1)
                    //{
                    //    dataGridView1.Rows.RemoveAt(selectedIndex);
                    //}
                    //if (sv.MaSo.ToString() == ms)
                    //{
                    //    StudentList.Remove(sv);
                    //    break;
                    //}
                    if(sv.MaSo.ToString() == ms)
                    {
                        StudentList.Remove(sv);
                        break;
                    }
                }
            }
            
            UpdateList();

        }
    }
}
