﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudent
{
    class SinhVienCaoDang : SinhVien
    {
        public SinhVienCaoDang() { }

        public SinhVienCaoDang(string DiaChi, string DienThoai, string HoTen, string MaSo,
            DateTime NgaySinh, string NienKhoa) :
            base(DiaChi, DienThoai, HoTen, MaSo, NgaySinh, NienKhoa)
        {

        }
        public override string LoaiHinh()
        {
            return "cao đẳng";
        }
    }
}
