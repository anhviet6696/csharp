﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStudent
{
    class SinhVienBangHai : SinhVien
    {
        public string Bang1 { get; set; }
        public string DonVi { get; set; }
        public SinhVienBangHai() { }

        public SinhVienBangHai(string DiaChi, string DienThoai, string HoTen, string MaSo,
            DateTime NgaySinh, string NienKhoa, string Bang1, string DonVi) : 
            base(DiaChi, DienThoai, HoTen, MaSo, NgaySinh, NienKhoa)
        {
            this.Bang1 = Bang1;
            this.DiaChi = DiaChi;
        }
        public override string LoaiHinh()
        {
            return "bằng hai";
        }

    }
}
