//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HopDongEMPHighLand.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contract
    {
        public int ID_Contract { get; set; }
        public string CMND { get; set; }
        public Nullable<int> ID_Type { get; set; }
        public Nullable<int> ID_Job { get; set; }
        public string ID_Place { get; set; }
        public Nullable<System.DateTime> TimeStart { get; set; }
        public Nullable<System.DateTime> TimeEnd { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual TypeOfJob TypeOfJob { get; set; }
        public virtual PlaceWork PlaceWork { get; set; }
        public virtual TypeOfTime TypeOfTime { get; set; }
    }
}
