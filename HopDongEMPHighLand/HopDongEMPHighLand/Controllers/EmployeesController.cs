﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HopDongEMPHighLand.Models;

namespace HopDongEMPHighLand.Controllers
{
    public class EmployeesController : Controller
    {
        private ManageEmployeeHighLandEntities3 db = new ManageEmployeeHighLandEntities3();

        // GET: Employees
        public ActionResult Index(string txt)
        {
            // chỉ hiển thị nhân viên đăng kí phỏng vấn 
            // nhân viên đã kí hợp đồng cần phải đăng nhập
            var listE = db.Employees.Where(p => p.Status == false && p.UserRight == false).Select(p => p);
            if (String.IsNullOrEmpty(txt))
            {
                return View(listE);
            }
            if (!String.IsNullOrEmpty(txt))
            {
                listE = db.Employees.Where(p => p.NameEmp.Contains(txt) && p.Status == false && p.UserRight == false);
            }
            return View(listE);
        }

        // GET: Employees/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CMND,NameEmp,Phone,BirthDay,Gender,Status,UserRight")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CMND,NameEmp,Phone,BirthDay,Gender,Status,UserRight")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




        // phan xu li login        
        public ActionResult UserLogin(string cmnd, string phone)
        {
            //Employee e = (Employee)Session["emp"];
            var l = db.Employees.Select(p => p).Where(p => p.CMND.Equals(cmnd) && p.Phone.Equals(phone)).FirstOrDefault();
            if (l.UserRight == true)
            {
                return RedirectToAction("Create", "Contracts");
            }
            //if (l.Status==true)
            //{
            //    EmployeesController c = new EmployeesController();
            //    return View(c.Details("201813363"));
            //}
            return RedirectToAction("Index");

        }

        
    }
}
