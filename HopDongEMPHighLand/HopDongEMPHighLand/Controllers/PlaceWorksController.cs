﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HopDongEMPHighLand.Models;

namespace HopDongEMPHighLand.Controllers
{
    public class PlaceWorksController : Controller
    {
        private ManageEmployeeHighLandEntities3 db = new ManageEmployeeHighLandEntities3();

        // GET: PlaceWorks
        public ActionResult Index()
        {
            return View(db.PlaceWorks.ToList());
        }

        // GET: PlaceWorks/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlaceWork placeWork = db.PlaceWorks.Find(id);
            if (placeWork == null)
            {
                return HttpNotFound();
            }
            return View(placeWork);
        }

        // GET: PlaceWorks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PlaceWorks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Place,NamePlace")] PlaceWork placeWork)
        {
            if (ModelState.IsValid)
            {
                db.PlaceWorks.Add(placeWork);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(placeWork);
        }

        // GET: PlaceWorks/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlaceWork placeWork = db.PlaceWorks.Find(id);
            if (placeWork == null)
            {
                return HttpNotFound();
            }
            return View(placeWork);
        }

        // POST: PlaceWorks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Place,NamePlace")] PlaceWork placeWork)
        {
            if (ModelState.IsValid)
            {
                db.Entry(placeWork).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(placeWork);
        }

        // GET: PlaceWorks/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlaceWork placeWork = db.PlaceWorks.Find(id);
            if (placeWork == null)
            {
                return HttpNotFound();
            }
            return View(placeWork);
        }

        // POST: PlaceWorks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PlaceWork placeWork = db.PlaceWorks.Find(id);
            db.PlaceWorks.Remove(placeWork);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
