﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HopDongEMPHighLand.Models;

namespace HopDongEMPHighLand.Controllers
{
    public class TypeOfJobsController : Controller
    {
        private ManageEmployeeHighLandEntities3 db = new ManageEmployeeHighLandEntities3();

        // GET: TypeOfJobs
        public ActionResult Index()
        {
            return View(db.TypeOfJobs.ToList());
        }

        // GET: TypeOfJobs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfJob typeOfJob = db.TypeOfJobs.Find(id);
            if (typeOfJob == null)
            {
                return HttpNotFound();
            }
            return View(typeOfJob);
        }

        // GET: TypeOfJobs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeOfJobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Job,NameJob,Salary")] TypeOfJob typeOfJob)
        {
            if (ModelState.IsValid)
            {
                db.TypeOfJobs.Add(typeOfJob);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(typeOfJob);
        }

        // GET: TypeOfJobs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfJob typeOfJob = db.TypeOfJobs.Find(id);
            if (typeOfJob == null)
            {
                return HttpNotFound();
            }
            return View(typeOfJob);
        }

        // POST: TypeOfJobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Job,NameJob,Salary")] TypeOfJob typeOfJob)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeOfJob).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeOfJob);
        }

        // GET: TypeOfJobs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfJob typeOfJob = db.TypeOfJobs.Find(id);
            if (typeOfJob == null)
            {
                return HttpNotFound();
            }
            return View(typeOfJob);
        }

        // POST: TypeOfJobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TypeOfJob typeOfJob = db.TypeOfJobs.Find(id);
            db.TypeOfJobs.Remove(typeOfJob);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
