﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HopDongEMPHighLand.Models;

namespace HopDongEMPHighLand.Controllers
{
    public class TypeOfTimesController : Controller
    {
        private ManageEmployeeHighLandEntities3 db = new ManageEmployeeHighLandEntities3();

        // GET: TypeOfTimes
        public ActionResult Index()
        {
            return View(db.TypeOfTimes.ToList());
        }

        // GET: TypeOfTimes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfTime typeOfTime = db.TypeOfTimes.Find(id);
            if (typeOfTime == null)
            {
                return HttpNotFound();
            }
            return View(typeOfTime);
        }

        // GET: TypeOfTimes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeOfTimes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Type,NameType")] TypeOfTime typeOfTime)
        {
            if (ModelState.IsValid)
            {
                db.TypeOfTimes.Add(typeOfTime);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(typeOfTime);
        }

        // GET: TypeOfTimes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfTime typeOfTime = db.TypeOfTimes.Find(id);
            if (typeOfTime == null)
            {
                return HttpNotFound();
            }
            return View(typeOfTime);
        }

        // POST: TypeOfTimes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Type,NameType")] TypeOfTime typeOfTime)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeOfTime).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeOfTime);
        }

        // GET: TypeOfTimes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfTime typeOfTime = db.TypeOfTimes.Find(id);
            if (typeOfTime == null)
            {
                return HttpNotFound();
            }
            return View(typeOfTime);
        }

        // POST: TypeOfTimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TypeOfTime typeOfTime = db.TypeOfTimes.Find(id);
            db.TypeOfTimes.Remove(typeOfTime);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
