﻿create database ManageEmployeeHighLand
use ManageEmployeeHighLand
create table TypeOfTime (ID_Type int primary key not null, NameType varchar (max))
--
create table TypeOfJob(ID_Job int primary key not null, NameJob varchar(max), Salary decimal(10,2))
--
create table Employee(CMND varchar(9) primary key not null, NameEmp varchar(max) not null ,Phone varchar (10), BirthDay datetime default(getdate()),
Gender bit not null ,Status bit default(0),UserRight bit default(0))
--userRight=0: nhan vien, =1 là giám đốc(chỉ =1 mới được tạo mới Hợp đồng)
create table PlaceWork (ID_Place char(3) primary key not null,NamePlace varchar(max))
--
create table Contract(ID_Contract int identity primary key not null, CMND varchar(9),ID_Type int,ID_Job int,ID_Place char(3),TimeStart datetime default(getdate()),
TimeEnd Datetime default(getdate()))
--
ALTER TABLE Contract	
ADD CONSTRAINT FK_CMND FOREIGN KEY(CMND)
References Employee(CMND)
--
ALTER TABLE Contract	
ADD CONSTRAINT FK_Type FOREIGN KEY(ID_Type)
References TypeOfTime(ID_Type)
--
ALTER TABLE Contract	
ADD CONSTRAINT FK_Job FOREIGN KEY(ID_Job)
References TypeOfJob(ID_Job)
--
ALTER TABLE Contract	
ADD CONSTRAINT FK_Place FOREIGN KEY(ID_Place)
References PlaceWork(ID_Place)

select * from PlaceWork
select * from TypeOfJob
select * from Employee
select * from TypeOfTime
select * from Contract
--
select Employee.CMND,TypeOfJob.NameJob,TypeOfTime.NameType,PlaceWork.NamePlace from Contract 
inner join Employee on  Employee.CMND=Contract.CMND
inner join TypeOfJob on TypeOfJob.ID_Job=Contract.ID_Job
inner join TypeOfTime on TypeOfTime.ID_Type=Contract.ID_Type
inner join PlaceWork on PlaceWork.ID_Place=Contract.ID_Place
where Contract.CMND='201813363'

ALTER TABLE Contract
DROP CONSTRAINT FK_CMND,FK_Type,FK_Job,FK_Place
drop table Employee,TypeOfJob,TypeOfTime,PlaceWork,Contract

insert into TypeOfTime(ID_Type,NameType) values (1,'Full time'), (2,'Part time')
insert into TypeOfJob (ID_Job,NameJob,salary) values (1,'Pha che', 1200000.0),(2,'Bao ve',1500000.0),(3,'Thu ngan', 1900000.0),(4,'Phuc vu', 1500000.0)
insert into PlaceWork (ID_Place,NamePlace) values ('DNG','Da Nang'), ('HCM','Ho CHi Minh'), ('YBA','Yen Bai')
insert into Employee(CMND , NameEmp  ,Phone ,Gender ) values ('201813363','Quoc Viet', '0349880019',1)
insert into Employee(CMND , NameEmp  ,Phone ,Gender ) values ('201116327','Quynh Tram', '0120226669',0)
insert into Employee(CMND , NameEmp  ,Phone ,Gender ) values ('201116363','Thuy Van', '0113640722',0)
insert into Employee(CMND , NameEmp  ,Phone ,Gender ) values ('201126325','Minh Trang', '0120880012',1)
insert into Employee(CMND , NameEmp  ,Phone ,Gender ) values ('admin','admin', 'admin',1)
