﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastFoodOrder
{
    public partial class Form1 : Form
    {
        DataTable dt = new DataTable();


        public Form1()
        {
            InitializeComponent();
            dt.Columns.Add("FoodName", typeof(string));
            dt.Columns.Add("Quantity", typeof(int));
            //DataRow r = dt.NewRow();
            //r["Quantity"] = 0;
            //dt.Rows.Add(r);
            dataGridView1.DataSource = dt;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Bugger Phô Mai Bò";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Bugger Phô Mai Bò")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Bugger Phô Mai Gà";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Bugger Phô Mai Gà")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Bugger Phô Mai Tôm";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Bugger Phô Mai Tôm")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Bugger Phô Mai Cá";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Bugger Phô Mai Cá")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Tôm viên Cola";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Tôm viên Cola")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Gà viên Cola";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Gà Viên Cola")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Gà rán phần";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Gà rán phần")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Cơm gà Tender";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Cơm gà Tender")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Pepsi";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Pepsi")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Coca";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Coca")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "7 up";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "7 up")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Lipton";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Lipton")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Cafe";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Cafe")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Cam";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Cam")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            DataRow r = dt.NewRow();
            foreach (DataRow i in dt.Rows)
            {
                if (i["FoodName"].ToString() == "")
                {
                    i["FoodName"] = "Khoai tây chiên";
                    i["Quantity"] = 1;
                }
                else if (i["FoodName"].ToString() == "Khoai tây chiên")
                {
                    int quantity = Convert.ToInt32(i["Quantity"]);
                    quantity += 1;
                    i["Quantity"] = quantity;
                    return;
                }
            }
            dt.Rows.Add(r);
        }
    }
}
