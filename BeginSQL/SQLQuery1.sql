create database BeginCSharp
use BeginCSharp
go
create table SV(MSSV int primary key not null,NameSV varchar(max),Gender bit,Birthday date default(getdate()),
DTB float, ID_Lop int,
FOREIGN KEY (ID_Lop) REFERENCES Lop(ID_Lop))
go
create table Lop(ID_Lop int primary key not null,NameLop varchar(50))
go
drop table SV
drop table Lop
go
insert  into Lop(ID_Lop,NameLop) values (1,'SE1301'),(2,'SE1302')
insert into SV(MSSV,NameSV,Gender,DTB,ID_Lop) values (101,'Tran Quoc Viet',1,9.9,1),(102,'Tran Chau',0,9.9,1),
(103,'Nguyen Van A',0,9.9,2)
go
select MSSV,NameSV,Gender,Birthday,DTB,SV.ID_Lop ,NameLop from SV inner join Lop on SV.ID_Lop=Lop.ID_Lop where NameLop='SE1302'
go
select * from SV
select * from Lop
go
update SV set NameSV='Anh viet day' where MSSV=103
go
select NameLop from Lop