﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeginSQL
{
    public class DBHelper
    {
        public SqlConnection cnn { get; set; }
        public DBHelper(string cnnstr)
        {
            this.cnn = new SqlConnection(cnnstr);
        }
        public DataTable GetRecord(string querry) // hàm để in table
        {
            //querry += reusultClass;
            DataTable dt = new DataTable();
            dt.Columns.Add("MSSV", typeof(int));
            dt.Columns.Add("NameSV", typeof(string));
            dt.Columns.Add("Gender", typeof(bool));
            dt.Columns.Add("DTB", typeof(double));
            dt.Columns.Add("Birthday", typeof(DateTime));
            dt.Columns.Add("ID_Lop", typeof(int));
            //dt.Columns.Add("Lop", typeof(string));
            SqlCommand cmd = new SqlCommand(querry, this.cnn);
            SqlDataReader r;
            this.cnn.Open();
            r = cmd.ExecuteReader();
            while (r.Read())
            {
                DataRow dr = dt.NewRow();
                dr["MSSV"] = r["MSSV"];
                dr["NameSV"] = r["NameSV"];
                dr["Gender"] = r["Gender"];
                dr["DTB"] = r["DTB"];
                dr["Birthday"] = r["Birthday"];
                dr["ID_Lop"] = r["ID_Lop"];
                dt.Rows.Add(dr);
            }
            this.cnn.Close();
            return dt;
        }
        public void InitDB(string querry)
        {
            SqlCommand cmd = new SqlCommand(querry, this.cnn);
            this.cnn.Open();
            cmd.ExecuteNonQuery();
            this.cnn.Close();
        }
    }
}
