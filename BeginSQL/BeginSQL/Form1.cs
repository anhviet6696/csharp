﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeginSQL
{
    public partial class Form1 : Form
    {
        public DBHelper db { get; set; }
        public Form1()
        {
            InitializeComponent();
            Load_ccbLoopFiter();
            comboBox1.SelectedIndex = 0; // gán giá trị mặc định cho combox là All
            string s = @"Data Source=DESKTOP-UOGK833;Initial Catalog=BeginCSharp;Persist Security Info=True;User ID=sa;Password=123";
            this.db = new DBHelper(s);
        }

        // kết nối CSDL bằng ADO.NET
        // Connection (SqlConnection,OLDBConnection) : lớp kết nối
        // SqlCommand : chứa các câu lệnh tương tác với CSDL
        // SqlDataReader: thành phần trung gian
        // SqlDataAdapter :(lớp cầu nối)0
        private void button1_Click(object sender, EventArgs e)
        {
            //SqlConnection cnn;
            //cnn.ConnectionString = @"Data Source=DESKTOP-UOGK833;Initial Catalog=BeginCSharp;Integrated Security=True";
            //cnn.Open();
            //// code tương tác vs CSDL
            //cnn.Close();
            ////AOD.NET->connected & disconnected mọi hành động thay đổi CSDL sẽ được thực hiện trong phần disconnect

            //cmd.Connection = cnn; // là đối tượng CSDL sẽ nhận thực hiện querry trên



            // trả về các bản ghi bị tác động (select count (*))
            //cmd.ExecuteScalar(); kiểu trả về là kiểu object nên phải ép kiểu 
            //string querry = "Select count (*) from SV";
            //SqlCommand cmd = new SqlCommand(querry, cnn);
            //cnn.Open();
            //int n = (int)cmd.ExecuteScalar();
            //cmd.CommandText = querry;
            //MessageBox.Show(n.ToString());
            //cnn.Close();



            ////trả về bản ghi bị tác động thay đổi : insert, delete, update

            //string rsClass= comboBox1.Items[comboBox1.SelectedIndex].ToString();
            //string querry;
            //if (comboBox1.Text == "All" )
            //{
            //    querry = "select MSSV,NameSV,Gender,DTB,Birthday,SV.ID_Lop ,NameLop from SV inner join Lop on SV.ID_Lop=Lop.ID_Lop";
            //}
            //else querry = "select MSSV,NameSV,Gender,DTB,Birthday,SV.ID_Lop ,NameLop from SV inner join Lop on SV.ID_Lop=Lop.ID_Lop where NameLop='"+rsClass+"'";
            //dataGridView1.DataSource = this.db.GetRecord(querry);
            //// đối tượng dataReader có nhược điểm là chỉ tồn tại trong khối open() và close()
            //// khi thoát ra khỏi đó nó sẽ được giải phóng

            //// Read():đọc từng bản ghi sau khi dữ liệu được đưa vào DataReader, đọc đến khi nào hết record thì return false

            string s = @"Data Source=DESKTOP-UOGK833;Initial Catalog=BeginCSharp;Persist Security Info=True;User ID=sa;Password=123";
            SqlConnection cnn = new SqlConnection(s);
            string querry = "select * from SV";
            SqlDataAdapter da = new SqlDataAdapter(querry, cnn);
            //DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            cnn.Open();
            //da.Fill(ds);
            da.Fill(dt);
            cnn.Close();
            //dataGridView1.DataSource = ds.Tables[0]; //dataSet đổ vào dataTable đầu tiên
            dataGridView1.DataSource = dt; // đổ dữ liệu vào dataTable
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
            //string querry = "insert into SV(MSSV,NameSV,Gender,DTB,ID_Lop) values (104,'Tran A',1,9.9,1)";
            //string querry = "Delete from SV where MSSV='104' ";
            string querry = "update SV set NameSV = 'Anh viet day' where MSSV = 101";
            this.db.InitDB(querry);
            
        }

        public void Load_ccbLoopFiter()
        {
            comboBox1.Items.Add("All");
            string s = @"Data Source=DESKTOP-UOGK833;Initial Catalog=BeginCSharp;Persist Security Info=True;User ID=sa;Password=123";
            SqlConnection cnn = new SqlConnection(s);
            string querry = "select NameLop from Lop";
            SqlCommand cmd = new SqlCommand(querry, cnn);
            SqlDataReader r;
            cnn.Open();

            r = cmd.ExecuteReader();
            while (r.Read())
            {
                comboBox1.Items.Add(r.GetValue(0).ToString());
            }

            cnn.Close();

        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string s = @"Data Source=DESKTOP-UOGK833;Initial Catalog=BeginCSharp;Persist Security Info=True;User ID=sa;Password=123";
            SqlConnection cnn = new SqlConnection(s);

        }
    }
}
