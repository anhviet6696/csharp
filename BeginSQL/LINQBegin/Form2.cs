﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINQBegin
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            DBQLSVDataContext db = new DBQLSVDataContext();
            //db.SVs : list cac doi tuong SV
            //sdb.Lops : list cac doi tuong Lop
            //querry: cau lenh select all
            //var listSV = from p in db.SVs select new { p.MSSV, p.NameSV ,p.Lop.NameLop}; // inner join
            ////method
            //var listSV1 = db.SVs.Select(p => new { p.MSSV, p.NameSV , p.Lop.NameLop });


            // cau lenh select Where
            //var listSV = from p in db.SVs where (p.NameSV == "anh viet day") select new { p.MSSV, p.NameSV, p.Lop.NameLop };
            //var listSV1 = db.SVs.Where(p => p.NameSV == "Anh viet day");// tra ve doi tuong SV all thuoc tinh
            var listSV2 = db.SVs.Where(p => p.NameSV == "anh viet day").Select(p => new { p.MSSV, p.NameSV, p.Lop.NameLop });
            dataGridView1.DataSource = listSV2;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btThem_Click(object sender, EventArgs e)
        {
            DBQLSVDataContext db = new DBQLSVDataContext();
            // ko cần add đối tượng lớp, mà yêu cầu phải có ID_Lop-->phải tồn tại bên List<Lop>
            SV s = new SV { MSSV = 106, NameSV = "Quoc Dung", Gender = true, Birthday = DateTime.Now, DTB = 1.1, ID_Lop = 1 };
            //add doi tuong sinh vien vao list SV cua linQ
            db.SVs.InsertOnSubmit(s);
            // đồng bộ tư LinQ về DB
            db.SubmitChanges();
            var listSV = db.SVs.Select(p => new { });


        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBQLSVDataContext db = new DBQLSVDataContext();
            int MSSV = 101;
            var s = db.SVs.Where(p => p.MSSV == MSSV).FirstOrDefault();
            // xóa trong linQ
            db.SVs.DeleteOnSubmit(s);
            //đồng bộ linQ về trong DB
            db.SubmitChanges();
            var listSV = db.SVs.Select(p => new { p.MSSV, p.NameSV, p.Lop.NameLop });
            dataGridView1.DataSource = listSV;
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            DBQLSVDataContext de = new DBQLSVDataContext();
            int MSSV = 102;
            var SVUp = de.SVs.Where(p => p.MSSV == MSSV).FirstOrDefault();
            SVUp.NameSV = "Tran Quoc Viet";
            SVUp.ID_Lop = 2;
            // đồng bộ từ linq về DB
            de.SubmitChanges();
            var listSV = de.SVs.Select(p => new { p.MSSV, p.NameSV, p.Lop.NameLop });
            dataGridView1.DataSource = listSV;
        }
    }
}
