﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLySinhVienByLINQ
{
    public partial class Form1 : Form
    {
        public delegate bool CompareObj(object o1, object o2);
        public void Sort(CompareObj cmp)
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            SV[] arr = db.SVs.ToArray();
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (cmp(arr[i], arr[j]))
                    {
                        SV temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }                 
                dgvListSV.DataSource = arr.ToList();
            }
        }

        public Form1()
        {
            InitializeComponent();
            LoadComboBoxFilter();
            cbbLopFiter.SelectedIndex = 0;
        }
        public void LoadComboBoxFilter()
        {
            //cbbLopFiter.Items.Add("All");
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            CBBItem all = new CBBItem { CValue = "0", CText = "All" };
            //cbbLop.Items.Add(all);
            cbbLopFiter.Items.Add(all);
            //var listClass = db.Lops.ToList();
            foreach (Lop i in db.Lops)
            {
                cbbLop.Items.Add(new CBBItem
                {
                    CText = i.NameLop,
                    CValue = i.ID_Lop.ToString()
                });

            }
            foreach (Lop i in db.Lops)
            {
                cbbLopFiter.Items.Add(new CBBItem
                {
                    CText = i.NameLop,
                    CValue = i.ID_Lop.ToString()
                });

            }
            //cbbLopFiter.DataSource = listClass;
            //cbbLopFiter.DisplayMember = "NameLop";
            //cbbLopFiter.ValueMember = "ID_Lop";

            //cbbLop.DataSource = listClass;
            //cbbLop.DisplayMember = "NameLop";
            //cbbLop.ValueMember = "ID_Lop";
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            string Lop = cbbLopFiter.Text;
            var listSV = db.SVs.Where(p => p.Lop.NameLop == Lop);
            dgvListSV.DataSource = listSV;
        }

        private void butSearch_Click(object sender, EventArgs e)
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            string Lop = cbbLopFiter.Text;
            string Name = txtSearch.Text;
            if (cbbLopFiter.Text == "All")
            {
                var listSV = db.SVs.Select(p => p);
                dgvListSV.DataSource = listSV;
            }
            else
            {
                var listSV1 = db.SVs.Where(p => p.Lop.NameLop == Lop && p.NameSV.Contains(Name)).Select(p => new
                {
                    p.SoTheSV,
                    p.NameSV,
                    p.DTB,
                    p.Birthday,
                    p.CMND,
                    p.HocBa,
                    p.THPT,
                    p.Gender,
                    p.Lop.NameLop
                }); ;
                dgvListSV.DataSource = listSV1;
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            foreach (SV i in db.SVs)
            {
                if (txtMaSo.Text == i.SoTheSV)
                {
                    MessageBox.Show("Trung MSSV");
                    break;
                }
                else if (rMale.Checked == false && rFemale.Checked == false)
                {
                    MessageBox.Show("Bê đê hay sao không nhập giới tính!");
                    break;
                }
                else
                {
                    db.SVs.InsertOnSubmit(new SV
                    {
                        SoTheSV = txtMaSo.Text,
                        NameSV = txtHoTen.Text,
                        ID_Lop = Convert.ToInt32(((CBBItem)cbbLop.SelectedItem).CValue),
                        DTB = Convert.ToDouble(txtDTB.Text),
                        Birthday = txtBirthDay.Value,
                        CMND = Convert.ToBoolean(ckbCMND.Checked),
                        HocBa = Convert.ToBoolean(ckbHocBa.Checked),
                        THPT = Convert.ToBoolean(ckbTHPT.Checked),
                        Gender = rMale.Checked
                    });
                    db.SubmitChanges();
                    Show();
                    break;
                }
            }
        }

        private void cbbLop_SelectedIndexChanged(object sender, EventArgs e) // hàm để lấy Text vav value của combobox Items
        {
            string Text = "";
            string Value = "";
            Text = ((CBBItem)cbbLop.SelectedItem).CText;
            Value = ((CBBItem)cbbLop.SelectedItem).CValue;
            //MessageBox.Show("NameLop=" + Text + "ID_Lop=" + Value);
        }

        private void dgvListSV_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            txtMaSo.ReadOnly = true;
            DataGridViewSelectedRowCollection r = dgvListSV.SelectedRows;
            if (r.Count >= 0) // có dòng được lựa chọn
            {
                string mssv = r[0].Cells["SoTheSV"].Value.ToString();
                foreach (SV i in db.SVs)
                {
                    if (i.SoTheSV == mssv)
                    {
                        txtMaSo.Text = i.SoTheSV;
                        txtHoTen.Text = i.NameSV;
                        cbbLop.Text = i.Lop.NameLop;
                        txtDTB.Text = (i.DTB).ToString();
                        //txtTel.Text = i.Tel;
                        txtBirthDay.Value = Convert.ToDateTime(i.Birthday); // 2 cái này cùng kiểu dữ liệu datetime
                        if (i.Gender == true)// giả sử male được chọn
                        {
                            rMale.Checked = true;
                        }
                        else rFemale.Checked = true;
                        ckbCMND.Checked = Convert.ToBoolean(i.CMND);
                        ckbHocBa.Checked = Convert.ToBoolean(i.HocBa);
                        ckbTHPT.Checked = Convert.ToBoolean(i.THPT);
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e) // ham Update
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            string MSSV = txtMaSo.Text;
            var SVUp = db.SVs.Where(p => p.SoTheSV == MSSV).FirstOrDefault();

            SVUp.SoTheSV = txtMaSo.Text;
            SVUp.NameSV = txtHoTen.Text;
            SVUp.ID_Lop = Convert.ToInt32(((CBBItem)cbbLop.SelectedItem).CValue);
            SVUp.DTB = Convert.ToDouble(txtDTB.Text);
            SVUp.Birthday = txtBirthDay.Value;
            SVUp.CMND = ckbCMND.Checked;
            SVUp.HocBa = ckbHocBa.Checked;
            SVUp.THPT = ckbTHPT.Checked;
            SVUp.Gender = rMale.Checked;

            db.SubmitChanges();
            //dgvListSV.DataSource = db.SVs.ToList();
            Show();
        }
        public void Show()
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            dgvListSV.DataSource = db.SVs.Select(p => new
            {
                p.SoTheSV,
                p.NameSV,
                p.DTB,
                p.Birthday,
                p.CMND,
                p.HocBa,
                p.THPT,
                p.Gender,
                p.Lop.NameLop
            }); ;
        }
        public void ClearTextBox()
        {
            txtMaSo.Clear();
            txtHoTen.Clear();
            cbbLop.Text = "";
            txtDTB.Clear();
            txtTel.Clear();
            txtBirthDay.Value = DateTime.Now;
            rMale.Checked = false;
            rFemale.Checked = false;
            ckbCMND.Checked = false;
            ckbHocBa.Checked = false;
            ckbTHPT.Checked = false;
            //txtMaSo.Enabled = true;
        }
        private void btDel_Click(object sender, EventArgs e) // hàm del
        {
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            string MSSV = txtMaSo.Text;
            var SVDel = db.SVs.Where(p => p.SoTheSV == MSSV).FirstOrDefault();
            // xóa trong linQ
            db.SVs.DeleteOnSubmit(SVDel);
            //đồng bộ linQ về trong DB
            db.SubmitChanges();
            Show();
        }


        private void btSort_Click(object sender, EventArgs e)
        {
            string inputSort = cbbSort.Text;
            DatabaseQLSVDataContext db = new DatabaseQLSVDataContext();
            switch (inputSort)
            {
                case "MSSV":
                    //var listMSSV = db.SVs.OrderByDescending(p => p.SoTheSV);
                    //dgvListSV.DataSource = db.SVs.OrderBy(p => p.SoTheSV).Select(p => new { p.SoTheSV });
                    Sort(DelegateSort.CompareMSSV);
                    break;
                case "Name":
                    //dgvListSV.DataSource = db.SVs.OrderByDescending(p => p.NameSV).Select(p => new { p.NameSV });
                    Sort(DelegateSort.CompareName);
                    break;
                case "Class":
                    dgvListSV.DataSource = db.SVs.OrderByDescending(p => p.Lop.NameLop).Select(p => new { p.Lop.NameLop });
                    break;
                case "DTB":
                    dgvListSV.DataSource = db.SVs.OrderByDescending(p => p.DTB).Select(p => new { p.DTB });
                    break;
                case "BirthDay":
                    dgvListSV.DataSource = db.SVs.OrderByDescending(p => p.Birthday).Select(p => new { p.Birthday });
                    break;
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            ClearTextBox();
            txtMaSo.ReadOnly = false;
        }
    }
}
