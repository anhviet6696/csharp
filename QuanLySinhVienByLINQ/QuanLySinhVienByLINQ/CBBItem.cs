﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVienByLINQ
{
    class CBBItem
    {
        public string CValue { get; set; }
        public string CText { get; set; }
        public override string ToString()
        {
            return this.CText;
        }
    }
}
