﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVienByLINQ
{
    class DelegateSort
    {
        public static bool CompareMSSV(object o1, object o2)
        {
            if (String.Compare(((SV)o1).SoTheSV, ((SV)o2).SoTheSV) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareName(object o1, object o2)
        {
            if (String.Compare(((SV)o1).NameSV, ((SV)o2).NameSV) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareLop(object o1, object o2)
        {
            if (String.Compare(((Lop)o1).NameLop, ((Lop)o2).NameLop) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareDTB(object o1, object o2)
        {
            if (((SV)o1).DTB > ((SV)o2).DTB)
            {
                return true;
            }
            return false;
        }
        public static bool CompareBirthDay(object o1, object o2)
        {
            if (DateTime.Compare(Convert.ToDateTime(((SV)o1).Birthday), Convert.ToDateTime(((SV)o2).Birthday)) > 0)
            {
                return true;
            }
            return false;
        }
       
    }
}
