﻿namespace QuanLySinhVienByLINQ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonReset = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.butAdd = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btDel = new System.Windows.Forms.Button();
            this.btSort = new System.Windows.Forms.Button();
            this.cbbSort = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.butReset = new System.Windows.Forms.Button();
            this.butSearch = new System.Windows.Forms.Button();
            this.dgvListSV = new System.Windows.Forms.DataGridView();
            this.cbbLopFiter = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rFemale = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBirthDay = new System.Windows.Forms.DateTimePicker();
            this.cbbLop = new System.Windows.Forms.ComboBox();
            this.txtDTB = new System.Windows.Forms.TextBox();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.txtMaSo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rMale = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ckbTHPT = new System.Windows.Forms.CheckBox();
            this.ckbHocBa = new System.Windows.Forms.CheckBox();
            this.ckbCMND = new System.Windows.Forms.CheckBox();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListSV)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(535, 262);
            this.buttonReset.Margin = new System.Windows.Forms.Padding(4);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(100, 28);
            this.buttonReset.TabIndex = 19;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(449, 229);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Tel";
            // 
            // butAdd
            // 
            this.butAdd.Location = new System.Drawing.Point(25, 23);
            this.butAdd.Margin = new System.Windows.Forms.Padding(4);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(100, 28);
            this.butAdd.TabIndex = 11;
            this.butAdd.Text = "Add";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(156, 25);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 28);
            this.button4.TabIndex = 12;
            this.button4.Text = "Update";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btDel
            // 
            this.btDel.Location = new System.Drawing.Point(296, 25);
            this.btDel.Margin = new System.Windows.Forms.Padding(4);
            this.btDel.Name = "btDel";
            this.btDel.Size = new System.Drawing.Size(100, 28);
            this.btDel.TabIndex = 13;
            this.btDel.Text = "Delete";
            this.btDel.UseVisualStyleBackColor = true;
            this.btDel.Click += new System.EventHandler(this.btDel_Click);
            // 
            // btSort
            // 
            this.btSort.Location = new System.Drawing.Point(436, 23);
            this.btSort.Margin = new System.Windows.Forms.Padding(4);
            this.btSort.Name = "btSort";
            this.btSort.Size = new System.Drawing.Size(100, 28);
            this.btSort.TabIndex = 14;
            this.btSort.Text = "Sort";
            this.btSort.UseVisualStyleBackColor = true;
            this.btSort.Click += new System.EventHandler(this.btSort_Click);
            // 
            // cbbSort
            // 
            this.cbbSort.FormattingEnabled = true;
            this.cbbSort.Items.AddRange(new object[] {
            "MSSV",
            "Name",
            "Class",
            "DTB",
            "BirthDay"});
            this.cbbSort.Location = new System.Drawing.Point(579, 26);
            this.cbbSort.Margin = new System.Windows.Forms.Padding(4);
            this.cbbSort.Name = "cbbSort";
            this.cbbSort.Size = new System.Drawing.Size(160, 24);
            this.cbbSort.TabIndex = 12;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.butAdd);
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Controls.Add(this.btDel);
            this.groupBox5.Controls.Add(this.btSort);
            this.groupBox5.Controls.Add(this.cbbSort);
            this.groupBox5.Location = new System.Drawing.Point(10, 536);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(827, 60);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Chức Năng";
            // 
            // butReset
            // 
            this.butReset.Location = new System.Drawing.Point(527, -27);
            this.butReset.Margin = new System.Windows.Forms.Padding(4);
            this.butReset.Name = "butReset";
            this.butReset.Size = new System.Drawing.Size(100, 28);
            this.butReset.TabIndex = 9;
            this.butReset.Text = "Reset";
            this.butReset.UseVisualStyleBackColor = true;
            // 
            // butSearch
            // 
            this.butSearch.Location = new System.Drawing.Point(660, 18);
            this.butSearch.Margin = new System.Windows.Forms.Padding(4);
            this.butSearch.Name = "butSearch";
            this.butSearch.Size = new System.Drawing.Size(100, 28);
            this.butSearch.TabIndex = 10;
            this.butSearch.Text = "Search";
            this.butSearch.UseVisualStyleBackColor = true;
            this.butSearch.Click += new System.EventHandler(this.butSearch_Click);
            // 
            // dgvListSV
            // 
            this.dgvListSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListSV.Location = new System.Drawing.Point(12, 69);
            this.dgvListSV.Margin = new System.Windows.Forms.Padding(4);
            this.dgvListSV.Name = "dgvListSV";
            this.dgvListSV.ReadOnly = true;
            this.dgvListSV.RowHeadersWidth = 51;
            this.dgvListSV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListSV.Size = new System.Drawing.Size(807, 142);
            this.dgvListSV.TabIndex = 12;
            this.dgvListSV.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvListSV_RowHeaderMouseClick);
            // 
            // cbbLopFiter
            // 
            this.cbbLopFiter.FormattingEnabled = true;
            this.cbbLopFiter.Location = new System.Drawing.Point(63, 22);
            this.cbbLopFiter.Margin = new System.Windows.Forms.Padding(4);
            this.cbbLopFiter.Name = "cbbLopFiter";
            this.cbbLopFiter.Size = new System.Drawing.Size(160, 24);
            this.cbbLopFiter.TabIndex = 11;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(436, 21);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(191, 22);
            this.txtSearch.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Lớp";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(485, 229);
            this.txtTel.Margin = new System.Windows.Forms.Padding(4);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(227, 22);
            this.txtTel.TabIndex = 18;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.butReset);
            this.groupBox4.Controls.Add(this.butSearch);
            this.groupBox4.Controls.Add(this.dgvListSV);
            this.groupBox4.Controls.Add(this.cbbLopFiter);
            this.groupBox4.Controls.Add(this.txtSearch);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(13, 297);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(827, 231);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Danh sách SV";
            // 
            // rFemale
            // 
            this.rFemale.AutoSize = true;
            this.rFemale.Location = new System.Drawing.Point(9, 68);
            this.rFemale.Margin = new System.Windows.Forms.Padding(4);
            this.rFemale.Name = "rFemale";
            this.rFemale.Size = new System.Drawing.Size(75, 21);
            this.rFemale.TabIndex = 1;
            this.rFemale.TabStop = true;
            this.rFemale.Text = "Female";
            this.rFemale.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtBirthDay);
            this.groupBox1.Controls.Add(this.cbbLop);
            this.groupBox1.Controls.Add(this.txtDTB);
            this.groupBox1.Controls.Add(this.txtHoTen);
            this.groupBox1.Controls.Add(this.txtMaSo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(24, 72);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(360, 218);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông Tin Sinh Viên";
            // 
            // txtBirthDay
            // 
            this.txtBirthDay.Location = new System.Drawing.Point(88, 185);
            this.txtBirthDay.Margin = new System.Windows.Forms.Padding(4);
            this.txtBirthDay.Name = "txtBirthDay";
            this.txtBirthDay.Size = new System.Drawing.Size(231, 22);
            this.txtBirthDay.TabIndex = 11;
            // 
            // cbbLop
            // 
            this.cbbLop.FormattingEnabled = true;
            this.cbbLop.Location = new System.Drawing.Point(88, 107);
            this.cbbLop.Margin = new System.Windows.Forms.Padding(4);
            this.cbbLop.Name = "cbbLop";
            this.cbbLop.Size = new System.Drawing.Size(231, 24);
            this.cbbLop.TabIndex = 10;
            this.cbbLop.SelectedIndexChanged += new System.EventHandler(this.cbbLop_SelectedIndexChanged);
            // 
            // txtDTB
            // 
            this.txtDTB.Location = new System.Drawing.Point(88, 143);
            this.txtDTB.Margin = new System.Windows.Forms.Padding(4);
            this.txtDTB.Name = "txtDTB";
            this.txtDTB.Size = new System.Drawing.Size(231, 22);
            this.txtDTB.TabIndex = 7;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(88, 74);
            this.txtHoTen.Margin = new System.Windows.Forms.Padding(4);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(231, 22);
            this.txtHoTen.TabIndex = 6;
            // 
            // txtMaSo
            // 
            this.txtMaSo.Location = new System.Drawing.Point(88, 34);
            this.txtMaSo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaSo.Name = "txtMaSo";
            this.txtMaSo.Size = new System.Drawing.Size(231, 22);
            this.txtMaSo.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 185);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ngày Sinh";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 146);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Điểm TB";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Lớp";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Họ Và Tên";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số Thẻ SV";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rFemale);
            this.groupBox3.Controls.Add(this.rMale);
            this.groupBox3.Location = new System.Drawing.Point(663, 72);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(151, 123);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Gender";
            // 
            // rMale
            // 
            this.rMale.AutoSize = true;
            this.rMale.Location = new System.Drawing.Point(8, 34);
            this.rMale.Margin = new System.Windows.Forms.Padding(4);
            this.rMale.Name = "rMale";
            this.rMale.Size = new System.Drawing.Size(59, 21);
            this.rMale.TabIndex = 0;
            this.rMale.TabStop = true;
            this.rMale.Text = "Male";
            this.rMale.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ckbTHPT);
            this.groupBox2.Controls.Add(this.ckbHocBa);
            this.groupBox2.Controls.Add(this.ckbCMND);
            this.groupBox2.Location = new System.Drawing.Point(423, 72);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(212, 140);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hồ Sơ SV";
            // 
            // ckbTHPT
            // 
            this.ckbTHPT.AutoSize = true;
            this.ckbTHPT.Location = new System.Drawing.Point(29, 102);
            this.ckbTHPT.Margin = new System.Windows.Forms.Padding(4);
            this.ckbTHPT.Name = "ckbTHPT";
            this.ckbTHPT.Size = new System.Drawing.Size(162, 21);
            this.ckbTHPT.TabIndex = 2;
            this.ckbTHPT.Text = "Bản Sao Bằng THPT";
            this.ckbTHPT.UseVisualStyleBackColor = true;
            // 
            // ckbHocBa
            // 
            this.ckbHocBa.AutoSize = true;
            this.ckbHocBa.Location = new System.Drawing.Point(29, 69);
            this.ckbHocBa.Margin = new System.Windows.Forms.Padding(4);
            this.ckbHocBa.Name = "ckbHocBa";
            this.ckbHocBa.Size = new System.Drawing.Size(134, 21);
            this.ckbHocBa.TabIndex = 1;
            this.ckbHocBa.Text = "Bản Sao Học Bạ";
            this.ckbHocBa.UseVisualStyleBackColor = true;
            // 
            // ckbCMND
            // 
            this.ckbCMND.AutoSize = true;
            this.ckbCMND.Location = new System.Drawing.Point(29, 33);
            this.ckbCMND.Margin = new System.Windows.Forms.Padding(4);
            this.ckbCMND.Name = "ckbCMND";
            this.ckbCMND.Size = new System.Drawing.Size(128, 21);
            this.ckbCMND.TabIndex = 0;
            this.ckbCMND.Text = "Bản Sao CMND";
            this.ckbCMND.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 602);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListSV)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btDel;
        private System.Windows.Forms.Button btSort;
        private System.Windows.Forms.ComboBox cbbSort;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button butReset;
        private System.Windows.Forms.Button butSearch;
        private System.Windows.Forms.DataGridView dgvListSV;
        private System.Windows.Forms.ComboBox cbbLopFiter;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rFemale;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker txtBirthDay;
        private System.Windows.Forms.ComboBox cbbLop;
        private System.Windows.Forms.TextBox txtDTB;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.TextBox txtMaSo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rMale;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ckbTHPT;
        private System.Windows.Forms.CheckBox ckbHocBa;
        private System.Windows.Forms.CheckBox ckbCMND;
    }
}

