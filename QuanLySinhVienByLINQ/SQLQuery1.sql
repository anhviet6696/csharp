create database QuanLySinhVien
use QuanLySinhVien
create table SV(SoTheSV varchar(50) primary key not null,NameSV varchar(max),ID_Lop int,DTB float,
Birthday date default(getdate()),CMND bit,HocBa bit,THPT bit,Gender bit
 
FOREIGN KEY (ID_Lop) REFERENCES Lop(ID_Lop))
go
create table Lop(ID_Lop int primary key not null,NameLop varchar(50))
go
insert  into Lop(ID_Lop,NameLop) values (1,'SE1301'),(2,'SE1302'),(3,'SE1303'),(4,'SE1304')
go
insert into SV(SoTheSV,NameSV,ID_Lop,DTB,CMND,HocBa,THPT,Gender) values ('DE130047','Tran Quoc Viet',1,9.9,1,1,1,1),
('DE130048','Q.Tram',2,9.9,1,1,1,0),('DE130049','Khanh Chgau',1,9.9,1,0,0,0)
select * from Lop
select * from SV
select * from SV inner join Lop on Lop.ID_Lop=SV.ID_Lop where NameLop='SE1301'  and NameSV like 'T%'
select * from SV order by SoTheSV DESC
