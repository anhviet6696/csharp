﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Events
{
    class DigitalClock
    {
        public void DK(Clock c)
        {
            c.OnsecondChange += new Clock.SecondHandler(this.ShowDC);
        }
        public void ShowDC(object o, EventArgs e)
        {
            DateTime d = DateTime.Now;// lấy thời gian hiện tại
            Console.WriteLine("AC :{0},{1},{2},{3}", d.Hour, d.Minute, d.Second, d.Millisecond);
        }
    }
}
