﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Clock c = new Clock();
            AnalogClock ac = new AnalogClock();
            DigitalClock dc = new DigitalClock();

            //cho delegate của sự kiện OnsecondChange của đối tượng c tham chiếu đến
            //1) hàm ShowAC() của đối tượng ac
            ac.DK(c);
            //2) hàm ShowDC() của đối tượng dc
            dc.DK(c);

            c.Run();
            Console.ReadKey();
        }
    }
}
