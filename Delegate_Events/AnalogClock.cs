﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Events
{
    class AnalogClock
    {
        //viết hàm đăng kí : mục đích để cho đối tượng delegate của sự kiện tham chiếu tới hàm ShowAC
        public void DK(Clock c)
        {
            c.OnsecondChange += new Clock.SecondHandler(this.ShowAC);
        }
        public void ShowAC(object o, EventArgs e)
        {
            DateTime d = DateTime.Now;// lấy thời gian hiện tại
            Console.WriteLine("AC :{0},{1},{2},{3}", d.Hour, d.Minute, d.Second, d.Millisecond);
        }
    }
}
