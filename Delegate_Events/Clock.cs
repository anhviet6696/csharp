﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Delegate_Events
{
    class Clock
    {
        //khai báo kiểu delegate gắn với sự kiện
        public delegate void SecondHandler(object o, EventArgs e);
        //khai báo sự kiện gắn với lớp Clock(cug phải gắn với kiểu delegate vừa khai báo)
        public event SecondHandler OnsecondChange;

        // sau mỗi giây thì sự kiện OnsecondChange phải phát sinh
        public void Run()
        {
            
            while (true)
            {
                Thread.Sleep(1000);
                if ( OnsecondChange != null)
                {
                    OnsecondChange(this, new EventArgs());
                }
            }
        }
    }
}
