﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
   public class Nguoi
    {
        public string maso;
        public string hoten;
        public string gioitinh;
        public Nguoi(string maso, string hoten, string gioitinh)
        {
            this.maso = maso;
            this.hoten = hoten;
            this.gioitinh = gioitinh;
        }
    }
}
