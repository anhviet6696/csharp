﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class NhanVien : Nguoi
    {
        private string bangcap;
        public NhanVien(string maso, string hoten, string gioitinh, string bangcap)
        : base(maso, hoten, gioitinh)
        {
            this.bangcap = bangcap;
        }
    }
}