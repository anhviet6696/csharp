﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BeginASPdotNET.Models;

namespace BeginASPdotNET.Controllers
{
    public class SinhViensController : Controller
    {
        private Entities db = new Entities();

        // GET: SinhViens

        public ActionResult Index(string txt,int Lop_ID=0) //view len man hinh danh sach cac sinh vien
        {
            // int ID_Lop=0 đối số mặc định(mặc định khi gọi hàm này ko truyên id mặ định bàng 0, có thể ko cần truyền giá cho id nhưng ưu tiên giá trị truyền vào)
            //var sinhViens = db.SinhViens.Include(s => s.LopSV);
            //txt -> tìm kiếm SV có NameSV chứa txt
            //Lop_ID -> tìm kiếm SV có ID_Lop==Lop_ID
            var listLop = db.LopSVs.Select(p => p);
            ViewBag.Lop_ID = new SelectList(listLop,"ID_Lop","NameLop");// truyền tên thuộc tính khóa chính ID_Lop,NameLop
            var listSV = db.SinhViens.Select(p => p);
            if (!String.IsNullOrEmpty(txt))
            {
                listSV = listSV.Where(p => p.NameSV.Contains(txt));
            }
            if (Lop_ID != 0)
            {
                listSV = listSV.Where(p => p.ID_Lop == Lop_ID);
            }
            return View(listSV);
            
        }

        // GET: SinhViens/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SinhVien sinhVien = db.SinhViens.Find(id);
            if (sinhVien == null)
            {
                return HttpNotFound();
            }
            return View(sinhVien);
        }

        // GET: SinhViens/Create
        public ActionResult Create()
        {
            ViewBag.ID_Lop = new SelectList(db.LopSVs, "ID_Lop", "NameLop");
            return View();
        }

        // POST: SinhViens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SotheSV,NameSV,Birthday,DTB,ID_Lop,CMND,HocBa,THPT,Gender")] SinhVien sinhVien)
        {
            if (ModelState.IsValid)
            {
                db.SinhViens.Add(sinhVien);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_Lop = new SelectList(db.LopSVs, "ID_Lop", "NameLop", sinhVien.ID_Lop);
            return View(sinhVien);
        }

        // GET: SinhViens/Edit/5
        public ActionResult Edit(string id) // tra ve trang view cua Client (MEthod Get)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SinhVien sinhVien = db.SinhViens.Find(id); // get doi tuong sinh vien can edit
            if (sinhVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Lop = new SelectList(db.LopSVs, "ID_Lop", "NameLop", sinhVien.ID_Lop);
            return View(sinhVien);
        }

        // POST: SinhViens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SotheSV,NameSV,Birthday,DTB,ID_Lop,CMND,HocBa,THPT,Gender")] SinhVien sinhVien)
        {
            //page update thong tin sinh vien tren Server (MEthod POST)
            if (ModelState.IsValid)
            {
                db.Entry(sinhVien).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_Lop = new SelectList(db.LopSVs, "ID_Lop", "NameLop", sinhVien.ID_Lop);
            return View(sinhVien);
        }

        // GET: SinhViens/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SinhVien sinhVien = db.SinhViens.Find(id);
            if (sinhVien == null)
            {
                return HttpNotFound();
            }
            return View(sinhVien);
        }

        // POST: SinhViens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SinhVien sinhVien = db.SinhViens.Find(id);
            db.SinhViens.Remove(sinhVien);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
