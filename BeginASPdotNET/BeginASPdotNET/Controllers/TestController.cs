﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BeginASPdotNET.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public  string Hello1()
        {
            return "Helo anh viet day"; 
        }
        public string HelloParameter(string name,string lop)
        {
            return HttpUtility.HtmlEncode(name + " " + lop);
        }
    }
}