﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DelegateForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void ShowText(string txt) // ham hien thi txtBox len f1
        {
            txtForm1.Text = txt;
        }
        private void btForm1_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.d = new Form2.MyDel(ShowText);
            f.Show();
        }
    }
}
