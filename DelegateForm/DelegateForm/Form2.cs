﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DelegateForm
{
    public partial class Form2 : Form
    {
        public delegate void MyDel(string s); // kieu du lieu cua delegate
        public MyDel d { get; set; } // doi tuong delegate dung de tham chieu toi cac ham khac
        public Form2()
        {
            InitializeComponent();
        }

        private void txtForm2_TextChanged(object sender, EventArgs e)
        {
            if (d != null) // kiem tra delegate da tham chieu toi ham nao chua
            {
                d(txtForm2.Text);
            }
        }
    }
}
