﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeginEntity
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            QuanLySinhVienEntities db = new QuanLySinhVienEntities();
            var listSV = db.SVs.Select(p=> new { p.SoTheSV,p.NameSV,p.Lop.NameLop}).ToList();
            //Select(p=> new { p.SoTheSV,p.NameSV,p.Lop.NameLop}) chỉ hiển thị 3 thuộc tính, các thuộc tính còn lại phải là Nullable
            // dùng ToList() để ép listSV thành danh sách,dataGridView1 chỉ hiển thị mảng các đối tượng hoặc list các SV full thuộc tính
            dataGridView1.DataSource = listSV;

            //add vào LINQ
            //đồng bộ từ LINQ về CSDL

        }
    }
}
