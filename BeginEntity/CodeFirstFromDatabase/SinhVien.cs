namespace CodeFirstFromDatabase
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SinhVien")]
    public partial class SinhVien
    {
        [Key]
        [StringLength(50)]
        public string SotheSV { get; set; }

        [Required]
        public string NameSV { get; set; }

        public DateTime? Birthday { get; set; }

        public double? DTB { get; set; }

        public int ID_Lop { get; set; }

        public bool? CMND { get; set; }

        public bool? HocBa { get; set; }

        public bool? THPT { get; set; }

        public bool? Gender { get; set; }

        public virtual LopSV LopSV { get; set; }
    }
}
