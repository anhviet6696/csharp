﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    class CreateDB:CreateDatabaseIfNotExists<QLSV>
        //DropCreateDatabaseAlways<QLSV>
        //DropCreateDatabaseIfModelChanges<QLSV>
    {
        protected override void Seed(QLSV context)
        {
            SV s1 = new SV { SotheSV = "001", NameSV = "NVA", ID_Lop = 1 };
            SV s2 = new SV { SotheSV = "002", NameSV = "NVB", ID_Lop = 1 };
            SV s3 = new SV { SotheSV = "003", NameSV = "NVC", ID_Lop = 2 };

            Lop l1 = new Lop { ID_Lop = 1, NameLop = "SE1301" };
            Lop l2 = new Lop { ID_Lop = 2, NameLop = "SE1302" };

            context.SVs.Add(s1);
            context.SVs.Add(s2);
            context.SVs.Add(s3);

            context.Lops.Add(l1);
            context.Lops.Add(l2);
        }
    }
}
