﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    [Table("LopSV")]
    public class Lop
    {
        [Key][Required]
        public int ID_Lop { get; set; }
        public string NameLop { get; set; }
        public virtual ICollection<SV> SVs { get; set; }
        public Lop() //listSV sẽ được khởi tạo sau khi khởi tạo Lop
        {
            this.SVs = new HashSet<SV>();
        }
    }
}
