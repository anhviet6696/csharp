namespace CodeFirst
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class QLSV : DbContext
    {
        
        public QLSV()
            : base("name=QLSV") 
        {
            Database.SetInitializer<QLSV>(new CreateDB());
        }
        public virtual DbSet<SV> SVs { get; set; }
        public virtual DbSet<Lop> Lops { get; set; }

    }

  
}