﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeFirst
{
    public partial class Form1 : Form
    {
        QLSV db { get; set; }
        public Form1()
        {
            InitializeComponent();
            db = new QLSV();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.SVs.ToList();
        }
    }
}
