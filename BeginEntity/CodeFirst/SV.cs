﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    [Table("SinhVien")] // dat ten SinhVien cho class SV trong sql server
    public class SV
    {
        [Key][Required][StringLength(50)]
        public string SotheSV { get; set; }
        [Required]
        public string NameSV { get; set; }
        public DateTime? Birthday { get; set; }
        public double? DTB { get; set; }
        [Required]
        public int ID_Lop { get; set; }
        [ForeignKey("ID_Lop")] // de day vì nó chỉ nhận thuộc tính quan hệ vs ở dưới
        // tên khóa ngoại trùng với tên thuộc tính
        public virtual Lop lop { get; set; }
        public bool? CMND { get; set; }
        public bool? HocBa { get; set; }
        public bool? THPT { get; set; }
        public bool? Gender { get; set; }
    }
}
