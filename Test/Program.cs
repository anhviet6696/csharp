﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    static class Program
    {
        
        static void Main()
        {
            Console.Write("K team \n"); // Sử dụng ký tự đặc biệt để xuống dòng
            //Console.WriteLine(5); // Sử dụng lệnh in ra màn hình có xuống dòng
            Console.Write(6.5f); // In ra giá trị nhưng không xuống dòng
            Console.Write(Environment.NewLine); // sử dụng lệnh xuống dòng
            Console.Write(true);

            Console.ReadLine();
        }
    }
}
