﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            //tạo column cho listview
            ColumnHeader c1 = new ColumnHeader();
            c1.Text="MSSV";
            ColumnHeader c2 = new ColumnHeader();
            c2.Text="Name";
            ColumnHeader c3 = new ColumnHeader();
            c3.Text = "Age";
            listView1.Columns.Add(c1);
            listView1.Columns.Add(c2);
            listView1.Columns.Add(c3);
            //tạo các row cho listView
            //ListViewItem i1 = new ListViewItem();
            //i1.Text = "1";
            //ListViewItem.ListViewSubItem sb1 = new ListViewItem.ListViewSubItem();
            //sb1.Text = "NVA";
            //ListViewItem.ListViewSubItem sb2 = new ListViewItem.ListViewSubItem();
            //sb2.Text = "21";
            //i1.SubItems.Add(sb1);
            //i1.SubItems.Add(sb2);
            //ListViewItem i2 = new ListViewItem();
            //i2.Text = "2";
            //ListViewItem.ListViewSubItem sb3 = new ListViewItem.ListViewSubItem();
            //sb3.Text = "NVB";
            //ListViewItem.ListViewSubItem sb4 = new ListViewItem.ListViewSubItem();
            //sb4.Text = "21";
            //i2.SubItems.Add(sb3);
            //i2.SubItems.Add(sb4);
            //ListViewItem i3 = new ListViewItem();
            //listView1.Items.Add(i1);
            //listView1.Items.Add(i2);
            List<SV> li = new List<SV>();
            li.Add(new SV { MSSV = "130047", Name = "NVA", Age = 32 });
            li.Add(new SV { MSSV = "130048", Name = "NVB", Age = 33 });
            li.Add(new SV { MSSV = "130049", Name = "NVC", Age = 34 });
            foreach(SV i in li)
            {
                ListViewItem i1 = new ListViewItem();
                i1.Text = i.MSSV;
                ListViewItem.ListViewSubItem sb1 = new ListViewItem.ListViewSubItem();
                sb1.Text = i.Name;
                ListViewItem.ListViewSubItem sb2 = new ListViewItem.ListViewSubItem();
                sb2.Text = i.Age.ToString();
                i1.SubItems.Add(sb1);
                i1.SubItems.Add(sb2);
                listView1.Items.Add(i1);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // chỉ cho phép 1 row được chọn cùng 1 thời điểm
            ListView.SelectedListViewItemCollection item;
            item = listView1.SelectedItems;
            foreach(ListViewItem i in item)
            {
                if (item.Count > 0)
                {
                    string MSSV = item[0].Text; // MSSV item[0].SubItems[0].Text;
                    string Name = item[0].SubItems[1].Text;
                    string Age = item[0].SubItems[2].Text;
                    MessageBox.Show(MSSV + "," + Name + "," + Age);
                }
            }
            
        }
    }
}
