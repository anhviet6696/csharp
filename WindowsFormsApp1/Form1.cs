﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBox1.Items.Add("haha");
            comboBox1.Items.AddRange(new object[]{"anh","viet","day" });
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    if (textBox1.Text != "")
        //    {
        //        int index = listBox1.Items.IndexOf(textBox1.Text);
        //        if(index >= 0)
        //        {
        //            MessageBox.Show("Duplicate");
        //        }
        //        else
        //        {
        //            listBox1.Items.Add(textBox1.Text);
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please Input");
        //    }
        //}

        // code cho combo box 
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                if(comboBox1.Items.Count == 0)
                {
                    comboBox1.Items.Add(textBox1.Text);
                }
                else
                {
                    int index = comboBox1.Items.IndexOf(textBox1.Text);
                    if(index >= 0)
                    {
                        comboBox1.SelectedIndex = index;
                    }
                    else
                    {
                        comboBox1.Items.Add(textBox1.Text);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please Input");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            //MessageBox.Show(listBox1.Items[index].ToString());
            if(index >=0)
            {
                listBox1.Items.RemoveAt(index);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            textBox1.Text = comboBox1.Items[index].ToString();
        }
    }
}
