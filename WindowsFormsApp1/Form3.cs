﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        DataTable dt = new DataTable();
        public Form3()
        {
            InitializeComponent();
            // cách 1: liên kết giữa datagridiew với list đối tượng
            //List<SV> li = new List<SV>();
            //li.Add(new SV { MSSV = "130047", Name = "NVA", Age = 32 ,Mark=1.0});
            //li.Add(new SV { MSSV = "130048", Name = "NVB", Age = 33 ,Mark=2.0});
            //li.Add(new SV { MSSV = "130049", Name = "NVC", Age = 34 ,Mark=3.0});

            //dataGridView1.DataSource = li;


            // cách 2: liên kết giữa datagridiew với đối tượng DataTable
            
            dt.Columns.Add("MSSV", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Age", typeof(int));
            dt.Columns.Add("Mark", typeof(double));
            //tạo row riêng và add vào dt
            DataRow r=dt.NewRow();
            r["MSSV"] = "13"; // có thể ghi r[0] tương ứng, r[1],...
            r["Name"] = "Viet";
            r["Age"] = 21;
            r["Mark"] = 12.2;
            dt.Rows.Add(r);
            //add trược tiếp row vào dt 
            dt.Rows.Add("1", "Viet", 12, 12.0);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewSelectedRowCollection dr = dataGridView1.SelectedRows;
            // đoạn code này để click vào  head in ra nguyên row
            //string MSSV = dr[0].Cells["MSSV"].Value.ToString();
            //string Name = dr[0].Cells["Name"].Value.ToString();
            //string Age = dr[0].Cells["Age"].Value.ToString();
            //string Mark = dr[0].Cells["Mark"].Value.ToString();
            //MessageBox.Show(MSSV + "," + Name + "," + Age + "," + Mark);


            // đoạn code này để thay đổi giá trị Mark khi click vào header
            string MSSV = dr[0].Cells["MSSV"].Value.ToString();
            foreach(DataRow i in dt.Rows)
            {
                if (i["MSSV"].ToString() == MSSV)
                {
                    i["Mark"] = 0.0;
                }
            }
            dataGridView1.DataSource = dt;
        }
    }
}
