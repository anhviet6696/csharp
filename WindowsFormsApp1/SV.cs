﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class SV
    {
        public string MSSV { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public double Mark { get; set; }
    }
}
