﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Security
{
    public partial class Form1 : Form
    {

        const string sPath = "C:/Users/tranq/source/csharp/Security/data.txt";

        public void ReadData()
        {
            try
            {
                try
                {
                    string[] data = System.IO.File.ReadAllLines(sPath.Trim());
                    foreach (string i in data)
                    {
                        listBox1.Items.Add(i);
                    }
                }
                catch { }
            }
            catch { }
        }
        public void SaveFile()
        {
            System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(sPath);
            foreach (var item in listBox1.Items)
            {
                SaveFile.WriteLine(item);
            }

            SaveFile.Close();
        }
        public Form1()
        {
            InitializeComponent();
            ReadData();
        }
        public int CountCharacter(string s)
        {
            s = txtInput.Text;
            int k = s.Length;
            return k;
        }
        private void button8_Click(object sender, EventArgs e)
        {
            clock c = new clock();
            if (txtInput.Text != "")
            {
                if (txtInput.Text == "1645" || txtInput.Text == "1689")
                {
                    txtInput.MaxLength = 4;
                    listBox1.Items.Add(c.Show() + " " + "Technicians");
                    btC_Click(sender, e);
                }
                else if (txtInput.Text == "8345")
                {
                    listBox1.Items.Add(c.Show() + " " + "Custodians");
                    btC_Click(sender, e);
                }
                else if (txtInput.Text == "9998" || txtInput.Text == "1006" ||
                    txtInput.Text == "1007" || txtInput.Text == "1008")
                {
                    listBox1.Items.Add(c.Show() + " " + "Scientist");
                    btC_Click(sender, e);

                }
                else
                {
                    listBox1.Items.Add(c.Show() + " " + "Restricted Access");
                    btC_Click(sender, e);
                }
            }
            else
            {
                MessageBox.Show("Please Input");
            }
            SaveFile();


        }

        private void button7_Click(object sender, EventArgs e)
        {
            Button but6 = (Button)sender;
            txtInput.Text = txtInput.Text + but6.Text;
        }

        private void bt1_Click(object sender, EventArgs e)
        {
            Button but1 = (Button)sender;
            txtInput.Text = txtInput.Text + but1.Text;
        }

        private void btC_Click(object sender, EventArgs e)
        {
            txtInput.Text = "";
        }

        private void bt2_Click(object sender, EventArgs e)
        {
            Button but2 = (Button)sender;
            txtInput.Text = txtInput.Text + but2.Text;
        }

        private void bt3_Click(object sender, EventArgs e)
        {
            Button but3 = (Button)sender;
            txtInput.Text = txtInput.Text + but3.Text;
        }

        private void bt4_Click(object sender, EventArgs e)
        {
            Button but4 = (Button)sender;
            txtInput.Text = txtInput.Text + but4.Text;
        }

        private void bt5_Click(object sender, EventArgs e)
        {
            Button but5 = (Button)sender;
            txtInput.Text = txtInput.Text + but5.Text;
        }

        private void bt8_Click(object sender, EventArgs e)
        {
            Button but8 = (Button)sender;
            txtInput.Text = txtInput.Text + but8.Text;
        }

        private void bt9_Click(object sender, EventArgs e)
        {
            Button but9 = (Button)sender;
            txtInput.Text = txtInput.Text + but9.Text;
        }

        private void bt0_Click(object sender, EventArgs e)
        {
            Button but0 = (Button)sender;
            txtInput.Text = txtInput.Text + but0.Text;
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            if (CountCharacter(txtInput.Text) > 4)
            {
                MessageBox.Show("Khong vuot qua 4 chu so");
                txtInput.Text = "";
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
