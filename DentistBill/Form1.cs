﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DentistBill
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public double TT()
        {
            double tong = 0;
            //
            if (ckCV.Checked == true)
                tong += 100;
            if(ckTT.Checked == true)
                tong += 1200;
            if (ckCHR.Checked == true)
                tong += 200;
            //nếu combobox được lựa chọn
            //tong+=(Convert.ToInt32(comboBox1.SelectedIndex.ToString()))*80;
            int SoRang = 0;
            if (comboBox1.SelectedIndex >= 0)
            {
                int index = comboBox1.SelectedIndex;
                SoRang = Convert.ToInt32(comboBox1.Items[index].ToString());
            }
            
            tong += SoRang * 80;
            return tong;
        }

        private void btTT_Click(object sender, EventArgs e)
        {
            //tính được số tiền (s)
            if(txtBN.Text == "")
            {
                MessageBox.Show("Please input Name");
            }
            txtTotal.Text = TT().ToString();
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        // anh viet day
    }
}
