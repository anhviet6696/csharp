﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class MainForm : Form
    {
        List<SV> listSV;
        public delegate bool CompareObj(object o1, object o2);

        // ham sort tong quat khong can truyen vao listSV[] vi da co o tren
        //static void Sort(object[] listSV, CompareObj cmp)
        //{
        //    for (int i = 0; i < listSV.Length; i++)
        //    {
        //        for (int j = i + 1; j < listSV.Length; j++)
        //        {
        //            if (cmp(listSV[i], listSV[j]))
        //            {
        //                object temp = listSV[i];
        //                listSV[i] = listSV[j];
        //                listSV[j] = temp;
        //            }
        //        }
        //    }

        //}
        public void Sort( CompareObj cmp)
        {
            SV [] arr=listSV.ToArray();
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (cmp(arr[i], arr[j]))
                    {
                        SV  temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
                listSV = arr.ToList();
            }

        }
        public MainForm()
        {
            InitializeComponent();
            CreateDB();
            Load_ccbLoopFiter();
            cbbLopFiter.SelectedIndex = 0;
            txtSearch.Text = "";
        }
        public void CreateDB()
        {
            listSV = new List<SV>();
            listSV.Add(new SV { MSSV = "DE130046", Name = "Tran Quoc Viet", Lop = "SE1304", DTB = 5, Tel = "01202266696", BirthDay = DateTime.Now, Gender = true, CMND = true, HocBa = true, THPT = true });
            listSV.Add(new SV { MSSV = "DE130045", Name = "Nguyen Tuan Anh", Lop = "SE1305", DTB = 5, Tel = "0349880019", BirthDay = DateTime.Now, Gender = false, CMND = false, HocBa = true, THPT = false });
            listSV.Add(new SV { MSSV = "DE130055", Name = "Tran Khanh Chau", Lop = "SE1306", DTB = 5, Tel = "05113640722", BirthDay = DateTime.Now, Gender = true, CMND = true, HocBa = true, THPT = true });
        }
        public void Load_ccbLoopFiter()
        {
            cbbLopFiter.Items.Add("All");
            foreach (SV i in listSV)
            {
                if (cbbLopFiter.Items.IndexOf(i.Lop) < 0)
                {
                    cbbLopFiter.Items.Add(i.Lop);
                }
            }
        }

        private void butSearch_Click(object sender, EventArgs e)
        {
            string textNameSV = txtSearch.Text;
            string lopSearch = cbbLopFiter.Items[cbbLopFiter.SelectedIndex].ToString();
            dgvListSV.DataSource = GetListSV( textNameSV, lopSearch);
        }
        
        public DataTable GetListSV(string Name,string Lop)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MSSV", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Lop", typeof(string));
            dt.Columns.Add("DTB", typeof(double));
            dt.Columns.Add("Tel", typeof(string));
            dt.Columns.Add("BirthDay", typeof(DateTime));
            dt.Columns.Add("Gender", typeof(bool));
            dt.Columns.Add("CMND", typeof(bool));
            dt.Columns.Add("HocBa", typeof(bool));
            dt.Columns.Add("THPT", typeof(bool));
            // 1) lấy danh sách sinh viên mới dựa vào lớp và txtSearch-> biến Name
            // 2) tạo đối tượng DataTable chứa danh sách SV mới đó
            // 3) Trả về đối tượng DataTable vừa tạo
            List<SV> r = new List<SV>();
            //View các đối tượng SV có cùng lớp
            if (Lop == "All")
            {
                r = listSV;
            }
            else
            {
                foreach(SV i in listSV)
                {
                    if (i.Lop == Lop)
                    {
                        r.Add(i);
                    }
                }
            }
            // trong danh sách hiện tại của view lọc ra các đối tượng SV có thuộc tính Name chứa string Name
            if (Name != "")
            {
                foreach (SV i in r)
                {
                    if (i.Name.Contains(Name)==true)
                    {
                        AddDT(dt, i);
                    }
                }
            }
            else
            {
                foreach(SV i in r)
                    AddDT(dt, i);
            }
            return dt;
        }
        public void AddDT(DataTable dt,SV i)
        {
            DataRow dr = dt.NewRow();
            dr["MSSV"] = i.MSSV;
            dr["Name"] = i.Name;
            dr["Lop"] = i.Lop;
            dr["DTB"] = i.DTB;
            dr["Tel"] = i.Tel;
            dr["BirthDay"] = i.BirthDay;
            dr["Gender"] = i.Gender;
            dr["CMND"] = i.CMND;
            dr["HocBa"] = i.HocBa;
            dr["THPT"] = i.THPT;
            dt.Rows.Add(dr);
        }

        private void butReset_Click(object sender, EventArgs e)
        {
            txtMaSo.Clear();
            txtHoTen.Clear();
            cbbLop.Text = "";
            txtDTB.Clear();
            txtTel.Clear();
            rMale.Checked = false;
            rFemale.Checked = false;
            ckbCMND.Checked = false;
            ckbHocBa.Checked = false;
            ckbTHPT.Checked = false;
        }

        private void butAdd_Click(object sender, EventArgs e) //hàm add
        {
            bool GenderCheck;
            if (rMale.Checked)
            {
                GenderCheck = true;
            }
            else
            {
                GenderCheck = false;
            }
            SV sv_add = new SV
            {
                MSSV = txtMaSo.Text,
                Name = txtHoTen.Text,
                Lop = cbbLop.Text,
                DTB = Convert.ToDouble(txtDTB.Text),
                Tel = txtTel.Text,
                BirthDay = txtBirthDay.Value,
                Gender = GenderCheck,
                CMND = ckbCMND.Checked,
                HocBa = ckbHocBa.Checked,
                THPT = ckbTHPT.Checked
            };
            bool check = false;
            foreach (SV i in listSV)
            {
                if (i.MSSV == sv_add.MSSV)
                {
                    check = true;
                    break;
                }
            }
            if (check)
            {
                MessageBox.Show("Duplicate!");
            }
            else
            {
                listSV.Add(sv_add);
            }
            cbbLopFiter.SelectedIndex = 0;
            txtSearch.Text = "";
            dgvListSV.DataSource = GetListSV( txtSearch.Text, cbbLopFiter.Items[cbbLopFiter.SelectedIndex].ToString());
        }

        private void dgvListSV_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtMaSo.ReadOnly = true;
            DataGridViewSelectedRowCollection r = dgvListSV.SelectedRows;
            if (r.Count >= 0) // có dòng được lựa chọn
            {
                string mssv = r[0].Cells["MSSV"].Value.ToString();
                foreach (SV i in listSV)
                {
                    if (i.MSSV == mssv)
                    {
                        txtMaSo.Text = i.MSSV;
                        txtHoTen.Text = i.Name;
                        cbbLop.Text = i.Lop;
                        txtDTB.Text = (i.DTB).ToString();
                        txtTel.Text = i.Tel;
                        txtBirthDay.Value = i.BirthDay; // 2 cái này cùng kiểu dữ liệu datetime
                        if (i.Gender == true)// giả sử male được chọn
                        {
                            rMale.Checked = true;
                        }
                        else rFemale.Checked = true;
                        ckbCMND.Checked = i.CMND;
                        ckbHocBa.Checked = i.HocBa;
                        ckbTHPT.Checked = i.THPT;
                    }
                }
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            txtMaSo.Clear();
            txtHoTen.Clear();
            cbbLop.SelectedIndex = 0;
            txtDTB.Clear();
            txtTel.Clear();
            rMale.Checked = false;
            rFemale.Checked = false;
            ckbCMND.Checked = false;
            ckbHocBa.Checked = false;
            ckbTHPT.Checked = false;
        }

        private void button5_Click(object sender, EventArgs e)// hàm xóa (xóa 1 hoặc nhiêu SV cùng lúc)
        {
            DataGridViewSelectedRowCollection r = dgvListSV.SelectedRows;
            if (r.Count >= 0)
            {
                foreach (DataGridViewRow i in r)
                {
                    for (int j = 0; j < listSV.Count; ++j)
                    {
                        if (i.Cells["MSSV"].Value.ToString() == listSV[j].MSSV)
                        {
                            listSV.RemoveAt(j);
                            break; // dùng break để 
                        }
                    }
                }
                List<SV> l = new List<SV>();
                l = listSV;
                cbbLopFiter.SelectedIndex = 0;
                txtSearch.Text = "";
                dgvListSV.DataSource = GetListSV( txtSearch.Text, cbbLopFiter.Items[cbbLopFiter.SelectedIndex].ToString());
            }

        }

        private void button4_Click(object sender, EventArgs e)// button update
        {
            string MSSV = txtMaSo.Text;
            bool check = false;
            bool GenderCheck;
            if (rMale.Checked)
            {
                GenderCheck = true;
            }
            else
            {
                GenderCheck = false;
            }
            foreach (SV i in listSV)
            {
                if (i.MSSV == MSSV)
                {
                    i.Name = txtHoTen.Text;
                    i.Lop = cbbLop.Text;
                    i.DTB = Convert.ToDouble(txtDTB.Text);
                    i.BirthDay = txtBirthDay.Value;
                    i.CMND = ckbCMND.Checked;
                    i.HocBa = ckbHocBa.Checked;
                    i.THPT = ckbTHPT.Checked;
                    i.Gender = GenderCheck;
                    i.Tel = txtTel.Text;
                }
            }
            if (check)
            {
                MessageBox.Show("");
            }
            dgvListSV.DataSource = GetListSV(txtSearch.Text, "All");
        }

        private void cbbSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void btSort_Click(object sender, EventArgs e)
        {
            //SV[] arr = listSV.ToArray();
            string check = cbbSort.Items[cbbSort.SelectedIndex].ToString();
            switch (check)
            {
                case "MSSV" :
                    Sort(SV.CompareMSSV);
                    dgvListSV.DataSource = listSV;
                    break;
                case "Name":
                    Sort(SV.CompareName);
                    dgvListSV.DataSource = listSV;
                    break;
                case "Class":
                    Sort(SV.CompareLop);
                    dgvListSV.DataSource = listSV;
                    break;
                case "DTB":
                    Sort(SV.CompareDTB);
                    dgvListSV.DataSource = listSV;
                    break;
                case "BirthDay":
                    Sort(SV.CompareName);
                    dgvListSV.DataSource = listSV;
                    break;
                case "Gender":
                    Sort(SV.CompareName);
                    dgvListSV.DataSource = listSV;
                    break;
            }
            //if (cbbSort.Text == "MSSV")
            //{
            //    Sort( SV.CompareMSSV);
            //    dgvListSV.DataSource = listSV;
            //}
            //if (cbbSort.Text == "Name")
            //{
            //    Sort( SV.CompareName);
            //    dgvListSV.DataSource = listSV;
            //}
            //if (cbbSort.Text == "Class")
            //{
            //    Sort( SV.CompareLop);
            //    dgvListSV.DataSource = listSV;
            //}
            //if (cbbSort.Text == "DTB")
            //{
            //    Sort( SV.CompareDTB);
            //    dgvListSV.DataSource = listSV;
            //}
            //if (cbbSort.Text == "BirthDay")
            //{
            //    Sort( SV.CompareBirthDay);
            //    dgvListSV.DataSource = listSV;
            //}
            //if (cbbSort.Text == "Gender")
            //{
            //    Sort( SV.CompareGender);
            //    dgvListSV.DataSource = listSV;
            //}

        }
    }
}
