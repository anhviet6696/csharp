﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class SV
    {
        public string MSSV { get; set; }
        public string Name { get; set; }
        public string Lop { get; set; }
        public double DTB { get; set; }
        public string Tel { get; set; }
        public DateTime BirthDay { get; set; }
        public bool Gender { get; set; }
        public bool CMND { get; set; }
        public bool HocBa { get; set; }
        public bool THPT { get; set; }

        public static bool CompareMSSV(object o1, object o2)
        {
            if (String.Compare(((SV)o1).MSSV, ((SV)o2).MSSV) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareName(object o1, object o2)
        {
            if (String.Compare(((SV)o1).Name, ((SV)o2).Name) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareLop(object o1, object o2)
        {
            if (String.Compare(((SV)o1).Lop, ((SV)o2).Lop) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareDTB(object o1, object o2)
        {
            if (((SV)o1).DTB> ((SV)o2).DTB)
            {
                return true;
            }
            return false;
        }
        public static bool CompareBirthDay(object o1, object o2)
        {
            if (DateTime.Compare(((SV)o1).BirthDay, ((SV)o2).BirthDay) > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CompareGender(object o1, object o2)
        {
            if (String.Compare(((SV)o1).Gender.ToString(), ((SV)o2).Gender.ToString()) > 0)
            {
                return true;
            }
            return false;
            //if (((SV)o1).Gender == ((SV)o2).Gender)
            //{
            //    return false;
            //}
            //else
            //    return true;
        }
    }
}
