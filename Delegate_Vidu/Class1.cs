﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Vidu
{
    class Class1
    {
        public delegate void My_Del(int x, int y);

        static void Sum(int a, int b)
        {
            Console.WriteLine("Sum ={0}", a + b);

        }
        static void Sub(int a, int b)
        {
            Console.WriteLine("Sum ={0}", a - b);

        }
        static void TT(int a, int b, My_Del del)
        {
             del(a, b);
        }
        private static void Main(string[] args)
        {
            My_Del d;
            d = new My_Del(Sum);

            d += new My_Del(Sub);

            d -= new My_Del(Sum);
            d(1, 2);
            Console.ReadKey();
        }
    }
}
