﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Vidu
{
    class Program
    {
        public delegate int My_Del(int x, int y);

        static int Sum(int a, int b)
        {
            return a + b;

        }
        static int Sub(int a, int b)
        {
            return a - b;

        }
        static int TT(int a, int b, My_Del del)
        {
            return del(a, b);
        }
        static void Main(string[] args)
        {
            //My_Del d; // khai báo đối tượng delegate d thuộc về kiểu My_Del
            //d = new My_Del(Sum);  khởi tạo đối tượng d tham chiếu đến hàm Sum
            My_Del d = new My_Del(Sum);
            int e1 = Sum(1, 2);
            //xem d như  là 1 phuong thức
            int e2 = d(1, 2);
            //xem d nhu là 1 đối tượng
            int e3 = d.Invoke(1, 2);
            Console.WriteLine("{0},{1},{2}", e1, e2, e3);
            Console.ReadKey();

            // dung cho ham TT
            int c = TT(1, 2, Sub);
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
